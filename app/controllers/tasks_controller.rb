class TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json
  # GET /tasks
  # GET /tasks.json
  def index
    @project = Project.find(params[:project_id])
    @tasks = @project.tasks.order("created_at DESC")
    respond_to do |format|
      format.json { render json: @tasks }
      format.html
    end
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
    respond_to do |format|
      format.json { render json: @task }
      format.html
    end
  end

  # GET /tasks/new


  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @project = Project.find(params[:project_id])
    @task = @project.tasks.new(task_params)
    respond_to do |format|
      if @task.save
        format.html
        format.json { render json: @task }
      else
        format.html
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    respond_to do |format|
      if @task.update(task_params)
        format.html
        format.json { render json: @task }
      else
        format.html
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task.destroy
    respond_to do |format|
      format.json { render json: @task }
      format.html
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @project = Project.find(params[:project_id])
      @task = @project.tasks.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:title, :description, :project_id, :estimated, :remaining, :worked, :status)
    end
end

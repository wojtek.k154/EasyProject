app.factory('Project', function ($resource) {
    return $resource("/projects/:id.json", { id: "@id"}, {
        'update': { method:'PUT' }
    });
});

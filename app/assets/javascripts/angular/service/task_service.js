app.factory('Task', function ($resource) {
    return $resource("/projects/:project_id/tasks/:id.json", {project_id: "@project_id", id: "@id"}, {
        'update': { method:'PUT' }
    });
});

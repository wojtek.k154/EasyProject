app.controller('ProjectCtrl', ['$scope', 'Project', function($scope, Project){

  $scope.projects = Project.query({status: "Created"});
  $scope.inprogres = Project.query({status: "InProgress"});
  //$scope.finished = Project.query({status: "Finished"});
  $scope.addProject = function(){
    Project.save({name: $scope.namep, description: $scope.descr }).$promise.then(
      function(success){
        $scope.projects.unshift(success);
        $scope.namep = "";
        $scope.descr = "";
        $scope.projectCreated = true;
        $scope.errors = null;
      },
      function(error){
        $scope.projectCreated = false;
        $scope.errors = error.data;
      }
    );
  };

  $scope.setInProgres = function(Id, index){
    Project.update({id: Id}, {status: "InProgress"}).$promise.then(
      function(success){
        $scope.inprogres.push(success);
        $scope.projects.splice(index, 1);
        $scope.projectUpdated = true;
        $scope.errors = null;
      },
      function(error){
        $scope.projectUpdated = false;
        $scope.errors = error.data;
      }
    );
  };

  $scope.setAsFinished = function(Id, index){
    Project.update({id: Id}, {status: "Finished"}).$promise.then(
      function(success){
        //$scope.finished.push(success);
        $scope.inprogres.splice(index, 1);
        $scope.projectUpdated = true;
        $scope.errors = null;
      },
      function(error){
        $scope.projectUpdated = false;
        $scope.errors = error.data;
      }
    );
  };
}]);

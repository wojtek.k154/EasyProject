angular.module("Panel",['rails']);
var app = angular.module("EMenager", ['ngResource', 'Panel']);

app.run(function($compile, $rootScope, $document) {
	return $document.on('page:load', function(){
		var body, finish;
		body = angular.element('body');
		finish = $compile(body.html())($rootScope);
		return body.html(finish);
	});
});

class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :description
      t.string :status, default: "Created"
      t.integer :project_id
      t.string :estimated
      t.string :remaining
      t.string :worked
      t.timestamps
    end
  end
end
